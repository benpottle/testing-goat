Provisioning a new site
=======================

## Required packages:

* nginx
* Python3
* Git
* pip
* virtualenv

eg, on Ubunutu:
    
    sudo apt-get install nginx git python3 python3-pip
    sudo pip3 install virtualenv

## Nginx Virtual Host config
* see nginx.template.conf
* replace SITENAME with, eg, staging.my-domain.com

## systemd service
* see gunicorn-systemd.template.service
* replace SITENAME with, eg, staging.my-domain.com

## Folder structure:
Assume we have a user account at /home/username

/home/username
  sites
    SITENAME
      database
      source
      static
      virtualenv
